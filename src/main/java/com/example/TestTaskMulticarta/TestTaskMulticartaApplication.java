package com.example.TestTaskMulticarta;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestTaskMulticartaApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestTaskMulticartaApplication.class, args);
	}

}

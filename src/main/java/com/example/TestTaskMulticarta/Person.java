package com.example.TestTaskMulticarta;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.xml.bind.annotation.XmlRootElement;

@JacksonXmlRootElement
@XmlRootElement(name="person")
@RequiredArgsConstructor
@NoArgsConstructor(force = true)
@Getter
@Setter
public class Person {

    private final String name;

    private final int age;
}
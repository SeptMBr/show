package com.example.TestTaskMulticarta;


import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractHttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.stereotype.Service;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.IOException;


public class SomeConverter<T> extends AbstractHttpMessageConverter<T> {
    private final Class<T> genericType;
    private final String xsdPath;

    private final SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

    public SomeConverter(Class<T> genericType, String xsdPath) {
        super(MediaType.APPLICATION_XML);
        this.genericType = genericType;
        this.xsdPath = xsdPath;
    }


    @Override
    protected boolean supports(Class<?> clazz) {
        return clazz.equals(Person.class);
    }

    @Override
    protected T readInternal(Class<? extends T> clazz, HttpInputMessage inputMessage) throws IOException, HttpMessageNotReadableException {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(genericType);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            Schema schema = schemaFactory.newSchema(new StreamSource(SomeConverter.class.getResourceAsStream(String.format("/%s.xsd", xsdPath))));
            jaxbUnmarshaller.setSchema(schema);
            return (T) jaxbUnmarshaller.unmarshal(inputMessage.getBody());
        } catch (Exception e) {
            throw new HttpMessageNotReadableException(e.getMessage(), e, inputMessage);
        }
    }

    @Override
    protected void writeInternal(T object, HttpOutputMessage outputMessage) throws IOException, HttpMessageNotWritableException {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(genericType);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.marshal(object, outputMessage.getBody());
        } catch (Exception e) {
            throw new HttpMessageNotWritableException(e.getMessage(), e);
        }
    }
}

package com.example.TestTaskMulticarta.purchase.adapter.driven.repository.jpa;


import lombok.*;

import javax.persistence.*;
import java.util.Date;


@Entity
@Table(name = "purchase", schema = "public")
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class PurchaseJpaEntity {
    @Id
    @Column(name = "id", nullable = false, updatable = false)
    @SequenceGenerator(name = "purchase_id", sequenceName = "purchase_id_seq", allocationSize = 1)
    public Long id;

    @Column(name = "name", nullable = false, updatable = false)
    public String name;

    @Column(name = "lastname", nullable = false, updatable = false)
    public String lastName;

    @Column(name = "age", nullable = false, updatable = false)
    public int age;

    @Column(name = "purchase_item_id", nullable = false, updatable = false)
    public Long purchase_item_id;

    @Column(name = "count", nullable = false, updatable = false)
    public int count;

    @Column(name = "amount", nullable = false, updatable = false)
    public double amount;

    @Column(name = "purchase_date", nullable = false, updatable = false)
    public Date date;
}



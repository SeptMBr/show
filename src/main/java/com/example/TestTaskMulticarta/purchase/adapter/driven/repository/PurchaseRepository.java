package com.example.TestTaskMulticarta.purchase.adapter.driven.repository;

import com.example.TestTaskMulticarta.purchase.adapter.driven.repository.jpa.IPurchaseJpaRepository;
import com.example.TestTaskMulticarta.purchase.adapter.driven.repository.jpa.PurchaseJpaEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@RequiredArgsConstructor
@Repository
public class PurchaseRepository{
    @Autowired
    public final IPurchaseJpaRepository repository;

    public List<PurchaseJpaEntity> getAll(){
        return repository.findAll();
    }

    public PurchaseJpaEntity getById(Long id){
        return repository.findById(id).orElse(null);
    }
    public PurchaseJpaEntity create(PurchaseJpaEntity purchaseJpaEntity){
        return repository.save(purchaseJpaEntity);
    }
    public void delete(Long id){
        repository.deleteById(id);
    }
    public PurchaseJpaEntity update(PurchaseJpaEntity purchaseJpaEntity){
        return repository.saveAndFlush(purchaseJpaEntity);
    }
}

package com.example.TestTaskMulticarta.purchase.adapter.driven.repository.jpa;

import org.springframework.data.jpa.repository.JpaRepository;

public interface IPurchaseJpaRepository extends JpaRepository<PurchaseJpaEntity, Long> {
}

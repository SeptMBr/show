package com.example.TestTaskMulticarta.purchase.adapter.driver;

import com.example.TestTaskMulticarta.Person;
import com.example.TestTaskMulticarta.purchase.adapter.driven.repository.PurchaseRepository;
import com.example.TestTaskMulticarta.purchase.adapter.driven.repository.jpa.PurchaseJpaEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.xml.bind.JAXBContext;
import java.util.List;

@RequiredArgsConstructor
@RestController
public class controller {
    @Autowired
    public final PurchaseRepository purchaseRepository;

    @GetMapping(
            value = "/",
            produces = "application/xml"
    )
    public Person showStatus() {
        return new Person("stas", 21);
    }

    @GetMapping(
            value = "/data"
    )
    public List<PurchaseJpaEntity> getData() {
        return purchaseRepository.getAll();
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping(
            value = "/",
            consumes = MediaType.APPLICATION_XML_VALUE
    )
    public String showStatus(@RequestBody Person person) {
        return "person";
    }
}

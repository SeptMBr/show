package com.example.TestTaskMulticarta.purchase.adapter.driver.api;

import com.example.TestTaskMulticarta.purchase.adapter.driven.repository.PurchaseRepository;
import com.example.TestTaskMulticarta.purchase.adapter.driven.repository.jpa.PurchaseJpaEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("purchase")
public class PurchaseController {
    @Autowired
    public final PurchaseRepository purchaseRepository;


    @GetMapping
    public List<PurchaseJpaEntity> getAll() {
        return purchaseRepository.getAll();
    }

    @GetMapping(
            value = "/{id}"
    )
    public PurchaseJpaEntity getById(
            @PathVariable("id") Long id
            ) {
        return purchaseRepository.getById(id);
    }

    @DeleteMapping(
            value = "/{id}"
    )
    public void deleteById(
            @PathVariable("id") Long id
            ) {
        purchaseRepository.delete(id);
    }

    @PutMapping(
            value = "/{id}"
    )
    public PurchaseJpaEntity update(
            @PathVariable("id") Long id,
            @RequestBody PurchaseJpaEntity purchaseJpaEntity
            ) {
        purchaseJpaEntity.id = id;
        return purchaseRepository.update(purchaseJpaEntity);
    }

    @PostMapping
    public PurchaseJpaEntity create(
            @RequestBody PurchaseJpaEntity purchaseJpaEntity
            ) {
        return purchaseRepository.create(purchaseJpaEntity);
    }
}

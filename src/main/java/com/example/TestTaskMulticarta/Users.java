package com.example.TestTaskMulticarta;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@RequiredArgsConstructor
@NoArgsConstructor(force = true)
@Getter
@Setter
public class Users {
    public final String username;
    public final String password;
}

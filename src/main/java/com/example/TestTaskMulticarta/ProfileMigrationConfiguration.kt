/*
package com.example.TestTaskMulticarta

import com.ytree.gi.config.db.DataBaseConfiguration
import org.flywaydb.core.Flyway
import org.postgresql.Driver
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.annotation.Order
import org.springframework.jdbc.datasource.SimpleDriverDataSource
import org.springframework.vault.core.VaultOperations

@Configuration
@Order(1)
class ProfileMigrationConfiguration(
  private val db: DataSourceProperties
) {

  @Bean(name = ["profile-flyway"], initMethod = "migrate")
  fun flyway(vaultOps: VaultOperations, dbConfig: DataBaseConfiguration): Flyway =
    Flyway.configure().also {
      it.dataSource(SimpleDriverDataSource(Driver(), db.url, db.username, db.password))
      it.baselineOnMigrate(true)
      it.schemas("profiles", "alm")
      it.table("flyway_schema_history")
      it.locations("db/migration", "com/ytree/gi/config/db/migrations")
      it.connectRetries(60) // 60 seconds
    }.load()
}
*/


CREATE TABLE IF NOT EXISTS public.purchase_item
(
    id             serial         NOT NULL        PRIMARY KEY,
    name		   varchar(128)   NOT NULL
);



CREATE TABLE IF NOT EXISTS public.purchase
(
    id                      SERIAL         NOT NULL        PRIMARY KEY,
    name		            varchar(128)   NOT NULL,
    lastname                varchar(128)   NOT NULL,
    age                     int            NOT NULL,
    purchase_item_id        BIGINT         NOT NULL        REFERENCES public.purchase_item(id),
    count                   int            NOT NULL,
    amount                  decimal        NOT NULL,
    purchase_date           date           NOT NULL
);

package com.example.TestTaskMulticarta.purchase.adapter.driven.repository;

import com.example.TestTaskMulticarta.purchase.adapter.driven.repository.jpa.IPurchaseJpaRepository;
import com.example.TestTaskMulticarta.purchase.adapter.driven.repository.jpa.PurchaseJpaEntity;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class PurchaseRepositoryTest {
    @Mock
    IPurchaseJpaRepository purchaseJpaRepository;
    @InjectMocks
    PurchaseRepository purchaseRepository;

    PurchaseJpaEntity purchaseJpaEntity = new PurchaseJpaEntity();
    List<PurchaseJpaEntity> purchaseJpaEntitys = List.of(purchaseJpaEntity);

    @Test
    void ShouldGetAllData(){
        Mockito.when(purchaseJpaRepository.findAll()).thenReturn(purchaseJpaEntitys);
        var a = purchaseRepository.getAll();

        assertEquals(a, purchaseRepository);
    }
}
package com.example.TestTaskMulticarta.purchase.adapter.driven.repository.jpa;

import com.example.TestTaskMulticarta.purchase.adapter.driven.repository.PurchaseRepository;
import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@RequiredArgsConstructor
class IPurchaseJpaRepositoryTest {
    @Autowired
    IPurchaseJpaRepository purchaseJpaRepository;


    @Test
    void ShouldGetAllData(){
        var a =purchaseJpaRepository.findAll();

        assertEquals(List.of(), a);
    }
}